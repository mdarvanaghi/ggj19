﻿Shader "GGJ19/outline"
{
    Properties
    {
        _Color ("Main Color", Color) = (1,1,1,1)
		_OutlineColor ("Outline Color", Color) = (0,0,0,1)
		_Outline ("Outline width",  Range(0,2)) = 0
        _XrayColor ("Xray color", Color) = (0,0,0,1)
        _Xray ("Enable/disable xray",Range(0,2)) = 0
        _ZTest ("Xray Z test", Float) = 1.0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
        Tags { "Queue" = "Transparent" }
        

         Pass { //OUTLINE PASS
            Tags { "RenderType"="Transparent" }
            ZTest Always
            ZWrite Off
 
            CGPROGRAM
 
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            
            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
 
            struct v2f {
                float4 pos : SV_POSITION;
            };
 
            float _Outline;
            float4 _OutlineColor;
 
            float4 vert(appdata v) : SV_POSITION {
                
                v.vertex.xyz *= _Outline;
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                return o.pos;
            }
 
            half4 frag(v2f i) : COLOR {
                return _OutlineColor;
            }
 
            ENDCG
        } //END OUTLINE PASS

        Pass //XRAY PASS
        {
            Tags { "LightMode" = "Always" }
			ZWrite Off
			ZTest [_ZTest]

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 pos : POSITION;
                float4 color : COLOR;
            };

            uniform float4 _XrayColor;
            uniform float _Xray;

            v2f vert (appdata v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.color = _XrayColor;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return i.color;
            }
            ENDCG
        } //END XRAY PASS

        Pass { //NORMAL PASS
            Tags {"LightMode"="ForwardBase"}
			ZWrite On

			CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc"
             #include "AutoLight.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR0;
                fixed3 ambient : COLOR1;
            };

            uniform float4 _Color;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                half n1 = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                o.color = n1 * _LightColor0;
                o.ambient = ShadeSH9(half4(worldNormal,1));
                TRANSFER_SHADOW(o)
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = _Color;
                fixed shadow = SHADOW_ATTENUATION(i);
                fixed3 lighting = i.color * shadow + i.ambient;
                col.rgb *= lighting;
                return col;
            }
            ENDCG
		}

        Pass //SHADOWCAST PASS
        {
            Tags {"LightMode"="ShadowCaster"}

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_shadowcaster
            #include "UnityCG.cginc"

            struct v2f { 
                V2F_SHADOW_CASTER;
            };

            v2f vert(appdata_base v)
            {
                v2f o;
                TRANSFER_SHADOW_CASTER_NORMALOFFSET(o)
                return o;
            }

            float4 frag(v2f i) : SV_Target
            {
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
}
