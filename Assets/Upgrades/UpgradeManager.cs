﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeManager : MonoBehaviour
{
    [Header("Upgrades")]
    public int money = 0;
    [Header("Loading rate")]
    public float boxesPerSecond = 1;
    public int loadRateCost = 10;
    public int bpsLevel = 1;
    [Header("Vans")]
    public int numberOfVehicles = 0;
    public int vanUpgradeCost = 20;
    public int vanLevel = 1;
    [Header("Capacity")]
    public int vanCapacity = 5;
    public int capacityUpgradeCost = 25;
    public int capLevel = 1;



    public UpgradePanel panel;
    public GameObject canvas;
    public List<PlayerVehicle> vehicles;

    public float radius =1;
    public GameObject vehiclePrefab;

    void Start()
    {

        GameObject[] g = GameObject.FindGameObjectsWithTag("Vehicle");
        for(int i = 0;i < g.Length; i++){
            vehicles.Add(g[i].GetComponent<PlayerVehicle>());
        }

        canvas.gameObject.SetActive(false);

        numberOfVehicles = g.Length;
    }

    public void StartUpgrade(int round){
        Time.timeScale = 0;

        panel.UpdateUI();

        canvas.gameObject.SetActive(true);
    }
    public void UpdateValues(){

        boxesPerSecond      = Settings.data.boxesPerSecond.Evaluate(bpsLevel);
        loadRateCost        = (int)Settings.data.boxesPerSecondPrice.Evaluate(bpsLevel);
        vanUpgradeCost      = (int)Settings.data.vehicleCost.Evaluate(vanLevel);
        vanCapacity         = (int)Settings.data.vehicleCapacity.Evaluate(capLevel);
        capacityUpgradeCost = (int)Settings.data.vehicleCapacityPrice.Evaluate(capLevel);
    }

    public void StopUpgrade(){
        Time.timeScale = 1;
        canvas.gameObject.SetActive(false);
        Settings.gameIsPaused = false;

        //add the things
        for(int i = vehicles.Count; i < numberOfVehicles; i ++){
            Vector3 p = new Vector3(Mathf.Sin(i)* radius ,0, Mathf.Cos(i) * radius); 
            GameObject g = Instantiate(vehiclePrefab, Vector3.zero, Quaternion.identity);
            vehicles.Add(g.GetComponent<PlayerVehicle>());

        }

        for(int i = 0 ;i < vehicles.Count; i ++){
            PlayerVehicle v = vehicles[i];

            v.capacity = vanCapacity;
            v.boxesPerSecond = boxesPerSecond;
        }

    }



}
