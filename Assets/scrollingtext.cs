﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrollingtext : MonoBehaviour
{
    public float speed;
    private Coroutine scrolling;
    private Vector3 startPos;

    void OnEnable(){
        startPos = this.transform.position;
        scrolling = StartCoroutine(StartScrolling());
        StartCoroutine(DisableAfterSeconds(12));
    }

    void OnDisable(){
        StopCoroutine(scrolling);
        this.transform.position = startPos;
    }
    IEnumerator DisableAfterSeconds(float seconds){
        yield return new WaitForSeconds(seconds);
        this.gameObject.SetActive(false); 
    }
    IEnumerator StartScrolling(){
        while(this.enabled){
            yield return new WaitForEndOfFrame();
            this.transform.position += Vector3.up*speed;
        }
    }
}
