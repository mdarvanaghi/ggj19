﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHolder : MonoBehaviour
{
    public enum AudioType{
        upgrades,
        cars,
        boxloading,
        boxunloading,
        birds,
        selecting,
        confirmlocation,
        drawing,
        gameover
    }
    public GameObject audioprefab;
    public AudioClip[] upgrades;
    public AudioClip[] cars;
    public AudioClip[] boxloading;
    public AudioClip[] boxunloading;
    public AudioClip[] birds;
    public AudioClip[] selecting;
    public AudioClip[] confirmlocation;
    public AudioClip[] drawing;

    public void play(AudioType type){
        int rand = 0;
        GameObject temp = Instantiate(audioprefab,this.transform.position,this.transform.rotation);
        temp.transform.parent = this.transform;
        AudioSource audiosource = temp.GetComponent<AudioSource>();
        switch (type)
        {
            case AudioType.upgrades:
            rand = (int)Random.Range(0,upgrades.Length);
            audiosource.clip = upgrades[rand];
            break;

            case AudioType.cars:
            rand = (int)Random.Range(0,cars.Length);
            audiosource.clip = cars[rand];
            break;

            case AudioType.boxloading:
            rand = (int)Random.Range(0,boxloading.Length);
            audiosource.clip = boxloading[rand];
            break;

            case AudioType.boxunloading:
            rand = (int)Random.Range(0,boxunloading.Length);
            audiosource.clip = boxunloading[rand];
            break;

            case AudioType.birds:
            rand = (int)Random.Range(0,birds.Length);
            audiosource.clip = birds[rand];
            break;

            case AudioType.selecting:
            rand = (int)Random.Range(0,selecting.Length);
            audiosource.clip = selecting[rand];
            break;

            case AudioType.confirmlocation:
            rand = (int)Random.Range(0,confirmlocation.Length);
            audiosource.clip = confirmlocation[rand];
            break;

            case AudioType.drawing:
            rand = (int)Random.Range(0,drawing.Length);
            audiosource.clip = drawing[rand];
            audiosource.volume = 0.1f;
            break;

            case AudioType.gameover:
            audiosource.clip = birds[10];
            break;

            default:
            break;
        }
        audiosource.Play();
    }
}
