﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{
    public SettingsData initData;
    public static SettingsData data;

    public static bool gameIsPaused = false;

    public void Awake(){
        data = initData;
    }
}
