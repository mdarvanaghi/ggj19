﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeAway : MonoBehaviour
{
    public Renderer rend;
    public float fadeOutDuration = 1f;
    private Material material;
    void Start()
    {
        material = rend.material;
        StartCoroutine(FadeOut(fadeOutDuration));
    }

    IEnumerator FadeOut(float duration)
    {
        float progress = 0f;
        while (progress < duration)
        {
            progress += Time.deltaTime;
            material.color = new Color(material.color.r, material.color.g, material.color.b, 1 - (progress / duration));
            yield return null;
        }
        Destroy(gameObject);
    }
}
