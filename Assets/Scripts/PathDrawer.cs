﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathDrawer : MonoBehaviour
{
    public Buildings buildings;
    // Pull from Jonas' grid generator after merge
    private float gridSpacing = 10f;
    public float cornerSnappingTolerance = 0.5f;
    [HideInInspector]
    public bool drawing = false;

    private float gridCornerLowerBound;
    private float gridCornerUpperBound;
    private InputHandler inputHandler;
    private LineRenderer lineRenderer;
    private AudioHolder audioHolder;

    void Start()
    {

        gridSpacing = buildings.spacing;
        inputHandler = GetComponentInParent<InputHandler>();
        lineRenderer = GetComponent<LineRenderer>();
        audioHolder = Camera.main.GetComponent<AudioHolder>();

        gridCornerLowerBound = gridSpacing - cornerSnappingTolerance;
        gridCornerUpperBound = gridSpacing + cornerSnappingTolerance;
    }

    public Vector3 SnapToGrid(Vector3 p){
        return new Vector3(Mathf.Round(p.x / gridSpacing) * gridSpacing, p.y, Mathf.Round(p.z / gridSpacing) * gridSpacing);
    }

    public IEnumerator StartDrawing(PlayerVehicle[] selectedVehicles)
    {
        if(Settings.gameIsPaused)goto end;
        drawing = true;
        List<Vector3> corners = new List<Vector3>();

        lineRenderer.positionCount = 2;
        Vector3 avgVehiclePos = new Vector3();
        foreach (PlayerVehicle vehicle in selectedVehicles)
        {
            avgVehiclePos += vehicle.transform.position;
            vehicle.ClearDestinations();
        }
        avgVehiclePos /= selectedVehicles.Length;

        lineRenderer.SetPosition(0, SnapToGrid(avgVehiclePos));


        while (Input.GetMouseButton(1))
        {
            if(Settings.gameIsPaused)goto end;
            /////////////////////////
            //TIME SCALE/////////////
            /////////////////////////
            if (Time.timeScale > inputHandler.timeScaleWhenDrawing)
            {
                Time.timeScale -= inputHandler.timeScaleFadeRate;
            }
            else
            {
                Time.timeScale = inputHandler.timeScaleWhenDrawing;
            }

            /////////////////////////
            //VEHICLE POSITION///////
            /////////////////////////
            avgVehiclePos = new Vector3();
            foreach (PlayerVehicle vehicle in selectedVehicles)
            {
                avgVehiclePos += vehicle.transform.position;
            }
            avgVehiclePos /= selectedVehicles.Length;

            //////////////////////////
            //DIRECTION CALCULATION///
            //////////////////////////
            Vector3 lineStartPos;
            if (corners.Count == 0) // Start from vehicle
            {
                lineStartPos = SnapToGrid(avgVehiclePos);
            }
            else // Start from previous corner
            {
                // print("Starting from corner");
                lineStartPos = corners[corners.Count - 1];
            }

            Vector3 lineStartToCursor = inputHandler.cursorPos - lineStartPos;
            Vector3 newLinePos;
            // Find closest value to grid line
            float scalarX = Vector3.Dot(lineStartToCursor, Vector3.right);
            float scalarZ = Vector3.Dot(lineStartToCursor, Vector3.forward);
            if (Mathf.Abs(scalarX) > Mathf.Abs(scalarZ))
            {
                newLinePos = new Vector3(lineStartPos.x + scalarX, lineStartPos.y, lineStartPos.z);
            }
            else
            {
                newLinePos = new Vector3(lineStartPos.x, lineStartPos.y, lineStartPos.z + scalarZ);
            }
            

            // Check for corner hit
            Vector3 closestCorner = SnapToGrid(newLinePos);
            lineRenderer.SetPosition(lineRenderer.positionCount - 1, closestCorner);
            if (!corners.Contains(closestCorner))
            {
                audioHolder.play(AudioHolder.AudioType.drawing);
                corners.Add(closestCorner);
                lineRenderer.positionCount++;
                lineRenderer.SetPosition(lineRenderer.positionCount - 1, closestCorner);
                foreach (PlayerVehicle vehicle in selectedVehicles)
                {
                    vehicle.AddDestination(closestCorner);
                }
            }
            yield return null;
        }
        Time.timeScale = 1f;
end:
        drawing = false;
    }
}
