﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "SettingsData", menuName = "SO/Settings", order = 1)]
public class SettingsData : ScriptableObject
{
    public List<Color> colors;

    [Header("Move Spawn (X = Round)")]
    public AnimationCurve boxesPerSpawn;
    public AnimationCurve delayBetweenSpawn;
    public AnimationCurve maxMoves;

    [Header("UpgradeManager (X = num purchases)")]
    public float moneyPerBox;

    public AnimationCurve boxesPerSecond;
    public AnimationCurve boxesPerSecondPrice;

    public AnimationCurve vehicleCost;

    public AnimationCurve vehicleCapacity;
    public AnimationCurve vehicleCapacityPrice;

    [Header("Game Manager(X = Round)")]
    public AnimationCurve roundGoal;
    public AnimationCurve roundTimeLimit;

}
