﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wrap : MonoBehaviour
{
    public Vector3 bounds;
    public float speed;
    public float randomSpeed;
    private float startSpeed;

    private bool isWrapping = true;

    void Start(){
        startSpeed = speed;
        speed += Random.Range(0,randomSpeed);
        transform.rotation = Quaternion.AxisAngle(Vector3.up, Random.Range(0,360));

    }
    void FixedUpdate()
    {
        transform.position += Vector3.right * speed * Time.deltaTime;

        if(transform.position.x > bounds.x){
            //wrap

            if(isWrapping) return;
            isWrapping = true;

            speed = startSpeed + Random.Range(0,randomSpeed);
            float z = Random.Range(-bounds.z,bounds.z);
            transform.position = new Vector3(-bounds.x, transform.position.y, z);
            transform.rotation = Quaternion.AxisAngle(Vector3.up, Random.Range(0,360));
        }else if(isWrapping){
            isWrapping = false;
        }
    }
}
