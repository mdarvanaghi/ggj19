﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerVehicle : MonoBehaviour
{
    private NavMeshAgent agent;
    public StatusPanel statusPanel;
    public bool selected;
    public float nextDestinationThreshold = 5f;
    public List<Vector3> destinationQueue;
    [Header("Stats")]
    public int capacity = 5;
    public int currentAmount = 0;
    public float boxesPerSecond = 1;
    public float nextBoxGetTime;


    public List<Box> boxesInCargo;

    public List<MeshRenderer> boxDummies;
    public List<Hover> boxDummiesHover;
    public GameObject boxDummyPrefab;

    public Vector3 boxDummySpawnPoint;
    public float boxDummySpacing;


    void Start()
    {
        boxesInCargo = new List<Box>(capacity);
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (destinationQueue.Count == 0 && agent.remainingDistance < nextDestinationThreshold/2)
            agent.Stop();

        if (agent.remainingDistance < nextDestinationThreshold)
        {
            if(destinationQueue.Count > 0){
                agent.destination = destinationQueue[0];
                destinationQueue.RemoveAt(0);
            }
        }
    }

    public void CarFull()
    {
    }

    public void ClearDestinations()
    {
        destinationQueue.Clear();
        agent.Stop();
        agent.ResetPath();
    }

    public void AddDestination(Vector3 destination)
    {
        destinationQueue.Add(destination);
        if(agent.isStopped)agent.Resume();
    }
    public void PickedUpBox(Box box)
    {
        currentAmount++;
        nextBoxGetTime = Time.time + (1 / boxesPerSecond);

        boxesInCargo.Add(box);

        GameObject g = Instantiate(boxDummyPrefab, Vector3.zero, Quaternion.identity);

        g.transform.SetParent(transform);
        g.transform.localPosition = boxDummySpawnPoint + (Vector3.up * boxDummySpacing * boxDummies.Count);
        g.transform.localRotation = Quaternion.identity;


        MeshRenderer m = g.GetComponent<MeshRenderer>();
        boxDummies.Add(m);
        boxDummiesHover.Add(g.GetComponent<Hover>());

        m.material.color = Settings.data.colors[box.type % Settings.data.colors.Count];


        if (capacity == currentAmount)
        {
            CarFull();
        }
    }

    public bool DeliverBoxOfType(int type){
        for(int i = 0 ; i < boxesInCargo.Count; i++){
            if(boxesInCargo[i].type == type){
                boxesInCargo.RemoveAt(i);
                nextBoxGetTime = Time.time + (1 / boxesPerSecond);
                GameLoop.instance.AddPoint();

                Destroy(boxDummies[i].gameObject);
                boxDummies.RemoveAt(i);
                boxDummiesHover.RemoveAt(i);

                for(int d = 0 ;d < boxDummies.Count;d++){
                    boxDummies[d].transform.localPosition = boxDummySpawnPoint + (Vector3.up * boxDummySpacing * d);
                    boxDummiesHover[d].NewHoverY();

                }

                currentAmount--;
                return true;
            }
        }
        return false;
    }
}
