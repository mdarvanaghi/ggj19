﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfter : MonoBehaviour
{
    public float seconds;
    float killTime;
    // Start is called before the first frame update
    void Start()
    {
        killTime = Time.time + seconds;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(killTime < Time.time) Destroy(gameObject);
    }
}
