﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropPoint : MonoBehaviour
{
    public int moveSpawnerIndex;
    public MoveSpawner moveSpawner;
    public int deliveredBoxes;
    public int maxBoxes;

    public int type;
    public List<PlayerVehicle> vehicles;

    [Header("Icon")]
    public GameObject icon;
    public Text iconText;
    private AudioHolder audioHolder;

    void Start()
    {
        audioHolder = Camera.main.GetComponent<AudioHolder>();
        vehicles = new List<PlayerVehicle>(10);
        deliveredBoxes = 0;

        //icon

        iconText.text = deliveredBoxes + "\n" + maxBoxes;

    }

    void FixedUpdate()
    {

        for(int i = 0; i < vehicles.Count; i++){
            PlayerVehicle v = vehicles[i];
            if(v.currentAmount > 0 &&
                v.nextBoxGetTime < Time.time){

                //take box
                if(v.DeliverBoxOfType(type)){
                    deliveredBoxes++;
                    audioHolder.play(AudioHolder.AudioType.boxunloading);
                    iconText.text = deliveredBoxes + "\n" + maxBoxes;
                    if(deliveredBoxes == maxBoxes){
                        moveSpawner.MoveComplete(type);
                        Destroy(icon);
                    }
                }

            }
        }
        
    }


    void OnTriggerEnter(Collider collider){

        if(collider.gameObject.tag == "Vehicle"){
            PlayerVehicle v = collider.GetComponent<PlayerVehicle>();
            vehicles.Add(v);
        }
    }

    void OnTriggerExit(Collider collider){
        if(collider.gameObject.tag == "Vehicle"){
            PlayerVehicle v = collider.GetComponent<PlayerVehicle>();
            vehicles.Remove(v);
        }
    }


}
