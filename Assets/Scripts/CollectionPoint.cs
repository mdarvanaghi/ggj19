﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionPoint : MonoBehaviour
{
    public MoveSpawner moveSpawner;

    public int type;

    public List<PlayerVehicle> vehicles;
    public List<GameObject> boxes;

    [Header("Icon")]
    public GameObject icon;
    public Text iconText;


    [Header("Spawning")]
    public GameObject prefab;
    public Vector3 spawnPoint;
    public int numBoxes;

    public int numStacks;

    public float spacing = 0.1f;

    public float random_spacing = 0.05f;
    public float random_rot_amount = 0.1f;

    private int layer = 0;
    
    [Header("Poof")]
    public GameObject poofPrefab;

    private float startTime;
    private Vector3 startPos;
    private AudioHolder audioHolder;

    // Start is called before the first frame update
    void Start()
    {

        vehicles = new List<PlayerVehicle>(10);
        vehicles = new List<PlayerVehicle>(numBoxes);
        audioHolder = Camera.main.GetComponent<AudioHolder>();

        //icon
        iconText.text = "" + numBoxes;


        //spawning
        for(int i = 0; i < numBoxes; i++){
            int s = (i+1) % numStacks;

            Vector3 p = Vector3.right * s;
            p += Vector3.right * s * (spacing + Random.Range(0, random_spacing));
            p += Vector3.up * layer;
            p += Vector3.up * layer * (spacing + Random.Range(0, random_spacing));
            p *= 0.5f;
            p += spawnPoint - Vector3.right * prefab.transform.localScale.x *0.5f;

            Quaternion q = Quaternion.AngleAxis(Random.Range(-random_rot_amount, random_rot_amount), Vector3.up);

            GameObject g = Instantiate(prefab, p, q) as GameObject;

            g.transform.SetParent(transform);

            boxes.Add(g);

            if((i + 1) % numStacks == 0){
                layer++;
            }
        }
        
    }

    void FixedUpdate()
    {

        for(int i = 0; i < vehicles.Count; i++){
            if(boxes.Count > 0){
                PlayerVehicle v = vehicles[i];
                if(v.capacity > v.currentAmount &&
                    v.nextBoxGetTime < Time.time){

                    //take box
                    RemoveBox();
                    v.PickedUpBox(new Box{type = type});

                }
            }else{
                //kys
                Destroy(icon);
            }
        }
        
    }


    void OnTriggerEnter(Collider collider){

        if(collider.gameObject.tag == "Vehicle"){
            PlayerVehicle v = collider.GetComponent<PlayerVehicle>();
            vehicles.Add(v);
        }
    }

    void OnTriggerExit(Collider collider){
        if(collider.gameObject.tag == "Vehicle"){
            PlayerVehicle v = collider.GetComponent<PlayerVehicle>();
            vehicles.Remove(v);
        }
    }


    void RemoveBox(){
        //spawn
        int i = Random.Range(0, boxes.Count);
        
        GameObject g = Instantiate(poofPrefab, boxes[i].transform.position, Quaternion.identity) as GameObject;
        audioHolder.play(AudioHolder.AudioType.boxloading);
        Destroy(boxes[i]);
        boxes.RemoveAt(i);

        iconText.text = "" + boxes.Count;
    }
}
