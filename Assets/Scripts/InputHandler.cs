using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class InputHandler : MonoBehaviour
{
    public float timeScaleWhenDrawing = 0.5f;
    public float timeScaleFadeRate = 0.1f;

    public Light cursorLight;
    public GameObject destinationMarker;
    public PlayerVehicle[] selectedVehicles;
    [HideInInspector]
    public Vector3 cursorPos;

    public enum InputMode
    {
        DrawPath,
        Click
    }
    public InputMode inputMode;
    public LayerMask mask;

    private PathDrawer pathDrawer;
    private AudioHolder audioHolder;

    void Start()
    {
        pathDrawer = GetComponentInChildren<PathDrawer>();
        audioHolder = Camera.main.GetComponent<AudioHolder>();
    }

    void Update()
    {
        if(Settings.gameIsPaused)return;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask))
        {
            cursorPos = hit.point;
            Vector3 lightPos = new Vector3(hit.point.x, cursorLight.transform.position.y, hit.point.z);
            cursorLight.transform.position = lightPos;
        }
        if (Input.GetMouseButtonDown(1))
        {
            if (inputMode == InputMode.DrawPath)
            {
                if (selectedVehicles.Length > 0 && !pathDrawer.drawing)
                {
                    StartCoroutine(pathDrawer.StartDrawing(selectedVehicles));
                }
            }
            else // Click navigation
            {
                foreach (PlayerVehicle vehicle in selectedVehicles)
                {
                    Instantiate(destinationMarker, hit.point, Quaternion.identity);
                    vehicle.AddDestination(hit.point);
                }
            }
        }
        if (Input.GetMouseButtonUp(1)){
            if(selectedVehicles.Length>0){
                audioHolder.play(AudioHolder.AudioType.confirmlocation);
            }
        }
    }
}
